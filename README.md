# Repositori del web

## TODO (abans de posar-nos a GitLab/Lektor)
 - [ ] Canviar els dest del script uploadpdf (i els missatges?)

## TODO
 - [ ] Posar fors a l'index(?)
 - [ ] El color que sobra al final del `404` despres del `footer`.
   - Canviar al style.css el color del body no funciona perquè el header es veu amb el fons fosc.
   - Aquí potser podriem fer que el header del 404 no fos translucid i el body de color.
 - [ ] Afegir al Ferran a quienes somos.
 - [ ] Lletra dinàmica a les imatges del apunts (projecte del Ferran).
 - [ ] Borrar ficheros innecesarios
 - [ ] Borrar historial de pdfs en cada push
 - [ ] Añadir acceso al repositorio de LatexFME

## Esborrar l'historial de commits des de git

Esborrar la carpeta `.git` pot portar problemes. Per esborrar l'historial i mantenir el codi:

```
# Check out to a temporary branch:
git checkout --orphan TEMP_BRANCH

# Add all the files:
git add -A

# Commit the changes:
git commit -am "Initial commit"

# Delete the old branch:
git branch -D master

# Rename the temporary branch to master:
git branch -m master

# Finally, force update to our repository:
git push -f origin master

# Track information for the current branch:
git branch --set-upstream-to=origin/master master
```

Sí, és un copy paste.

## Crear més avatars

Si cal incorporar algú nou a l'equip d'ApuntsFME, els actuals estan basats en els que trobareu [aquí](https://www.flaticon.com/packs/kids-avatars). Cal editar el fitxer ```.svg``` (per tal que es vegi amb bona definició), podeu fer-ho amb el programa *Inkscape*.

### Més informació
Per més informació, llegir [aquesta web](https://gist.github.com/heiswayi/350e2afda8cece810c0f6116dadbe651l) o [Google](https://www.google.es/).
